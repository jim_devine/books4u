package com.example.books4u;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.InputFilter;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EditProfile extends Activity {
	
	DBAdapter myDb;
	ImageView pic;
	SharedPreferences sp;
	public static final String filename = "SharedPrefs";
	String user_name;
	long rowId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_profile);
		
		openDB();
		
		//Cursor c = myDb.getAllRows(3);
		
		 sp = getSharedPreferences(filename, 0);
		  user_name = sp.getString("name", null);
		final TextView t1 = (TextView) findViewById(R.id.textView2);
		final TextView t2 = (TextView) findViewById(R.id.textView4);
		final TextView t3 = (TextView) findViewById(R.id.textView6);
		final TextView t4 = (TextView) findViewById(R.id.textView8);
		
		pic = (ImageView) findViewById(R.id.imageView2);
		Cursor c = myDb.getAllRows(1);
		 
		
		if(c.moveToFirst()){
			  
			  do{
				  if(c.getString(1).equals(user_name)){
					  
					  rowId = c.getLong(0);
					  t1.setText(user_name);
					  t2.setText(c.getString(2));
					  t3.setText(c.getString(3));
					  t4.setText(c.getString(4));
					  
						byte[] img1 = c.getBlob(5);
						
						
						Bitmap b1=BitmapFactory.decodeByteArray(img1, 0, img1.length);
						pic.setImageBitmap(b1);
					  
				  }
				  
				  
				  
			  }while(c.moveToNext());
		  }
	
		Button home = (Button)findViewById(R.id.home_button);
		Button logout = (Button)findViewById(R.id.logout);
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
	
	/*	if (c.moveToFirst()) {
			
			byte[] b = c.getBlob(1);
			InputStream is = new ByteArrayInputStream(b);
			pic.setImageBitmap(BitmapFactory.decodeStream(is));

			t1.setText(c.getString(2).toLowerCase());
			t2.setText(c.getString(3).toLowerCase());
			t3.setText(c.getString(4).toLowerCase());
			t4.setText(c.getString(5).toLowerCase());
		}*/
		
		final Button desc = (Button) findViewById(R.id.button4);
		desc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				popUp(t4.getText().toString(), t4, "desc");
				
				String change = t4.getText().toString();
				
				System.out.println("change: " + change);
				
				/*if(!change.equals("")){
					System.out.println("change in :" + change);
					
					Cursor c = myDb.getRow(rowId, 1);
					
					myDb.updateRow(rowId, c.getString(1), c.getString(2), c.getString(3), change, c.getBlob(5));
					
				}*/
			}
		});
		
		final Button name = (Button) findViewById(R.id.button1);
		name.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				popUp(t1.getText().toString(), t1, "name");
				//String change = t1.getText().toString().trim();
				/*if(!change.equals("")){
					Cursor c = myDb.getRow(rowId, 1);
					
					myDb.updateRow(rowId, change, c.getString(2), c.getString(3), c.getString(4), c.getBlob(5));
					
				}*/
			}
		});
		
		final Button email = (Button) findViewById(R.id.button2);
		email.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				popUp(t2.getText().toString(), t2, "email");
				//String change = t2.getText().toString().trim();
				/*if(!change.equals("")){
					Cursor c = myDb.getRow(rowId, 1);
					
					myDb.updateRow(rowId, c.getString(1), change, c.getString(3), c.getString(4), c.getBlob(5));
					
				}*/
			}
		});
		
		final Button pass = (Button) findViewById(R.id.button3);
		pass.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				popUp(t3.getText().toString(), t3, "pass");
				/*String change = t3.getText().toString().trim();
				
				if(!change.equals("")){
					Cursor c = myDb.getRow(rowId, 1);
					
					myDb.updateRow(rowId, c.getString(1), c.getString(2), change, c.getString(4), c.getBlob(5));
					
				}*/
			}
		});
		
		Button camera = (Button) findViewById(R.id.camera);
		camera.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					
					Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);     
					startActivityForResult(intent, 0);				
			}
		});
		
		Button back = (Button) findViewById(R.id.button5);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					
					finish();			
			}
		});
	}
	
	
	 @Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		   super.onActivityResult(requestCode, resultCode, data);
	            //this is the picture that you have just taken
		      Bitmap b = (Bitmap) data.getExtras().get("data");
		      pic.setImageBitmap(b);
		      
		      ByteArrayOutputStream bos=new ByteArrayOutputStream();
		        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
		        byte[] img=bos.toByteArray();  
		        
		        Cursor c = myDb.getAllRows(1);
		        
		       if(c.moveToFirst()){
		    	   do{
		    		   if(c.getString(1).equals(user_name)){
		    			   
		    			   long rowId = c.getLong(0);
		    			   
		    			   myDb.updateRow(rowId, user_name, c.getString(2), c.getString(3), c.getString(4), img);
		    			   
		    		   }
		    		   
		    	   }while(c.moveToNext());
		    	   
		    	   
		       }
		        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_profile, menu);
		return true;
	}
	
	public void popUp(final String field, final TextView t, final String change){
		final AlertDialog.Builder alert = new AlertDialog.Builder(this);
		
		
		alert.setTitle("Edit " + field);
		
		
		
		final EditText input = new EditText(this);
		input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(140) });
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
			
			final String value = input.getText().toString().trim();
			
			Cursor c = myDb.getRow(rowId, 1);
			
			if(change.equals("desc")){
				
				
				
				myDb.updateRow(rowId, c.getString(1), c.getString(2), c.getString(3), value, c.getBlob(5));
				
				Toast.makeText(EditProfile.this, "Successfully Updated ", Toast.LENGTH_SHORT).show();
			}
			else if(change.equals("name")){
				
				myDb.updateRow(rowId, value, c.getString(2), c.getString(3), c.getString(4), c.getBlob(5));
				Editor editor = sp.edit();
				editor.putString("name", value);
				editor.commit();
				Toast.makeText(EditProfile.this, "Successfully Updated ", Toast.LENGTH_SHORT).show();
			}
			else if (change.equals("pass")){
				
				myDb.updateRow(rowId, c.getString(1), c.getString(2), value, c.getString(4), c.getBlob(5));
				Toast.makeText(EditProfile.this, "Successfully Updated ", Toast.LENGTH_SHORT).show();
			}
			else{
				myDb.updateRow(rowId, c.getString(1), value, c.getString(3), c.getString(4), c.getBlob(5));
				Toast.makeText(EditProfile.this, "Successfully Updated ", Toast.LENGTH_SHORT).show();
			}
			
				System.out.println("value "+ value);
				t.setText(value);
        		//Toast.makeText(EditProfile.this, "Successfully Updated " + field, Toast.LENGTH_SHORT).show();
        		
		
		
		}
		}); 

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
			
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}


}
