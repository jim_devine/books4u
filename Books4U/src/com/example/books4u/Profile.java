package com.example.books4u;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile extends Activity {

	DBAdapter myDb;
	SharedPreferences sp;
	TextView description, name;
	ImageView pic;
	String username;
	public static final String filename = "SharedPrefs";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
		openDB();
		
		 sp = getSharedPreferences(filename, 0);
		 
		
		
		 pic = (ImageView) findViewById(R.id.imageView2);
		 name = (TextView) findViewById(R.id.textView3);
		
		
		TextView sold = (TextView) findViewById(R.id.textView2);
		TextView bought = (TextView) findViewById(R.id.TextView04);
		TextView current = (TextView) findViewById(R.id.TextView03);
		 description = (TextView) findViewById(R.id.textView4);
		
		 
		 display();
		 
		
		
		
		Button listings = (Button) findViewById(R.id.Button01);
		listings.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.MYLISTINGS");
				startActivity(openStartingPoint);
			}
		});
		
		Button mail = (Button) findViewById(R.id.mail);
		mail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.MYMESSAGES");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		Button back = (Button) findViewById(R.id.button_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		Button edit = (Button) findViewById(R.id.button_edit);
		edit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.EDITPROFILE");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		
		Button home = (Button)findViewById(R.id.home_button);
		Button logout = (Button)findViewById(R.id.logout);
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					//Intent intent = new Intent("com.example.books4u.HOME");
					//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					//startActivity(intent);
				finish();
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
				
	}
	
	public void display(){
		Cursor c = myDb.getAllRows(1);
		 username = sp.getString("name", null);
		 System.out.println("username" + username);
		 name.setText(username);
		 
		if (c.moveToFirst()) {			
			/*byte[] b = c.getBlob(1);
			InputStream is = new ByteArrayInputStream(b);
			pic.setImageBitmap(BitmapFactory.decodeStream(is));*/
			do{
				if(c.getString(1).equals(username)){
					String desc = c.getString(4);
					
					System.out.println("description " + desc);
					
					description.setText(desc);
					
					byte[] img1 = c.getBlob(5);
					
					
					Bitmap b1=BitmapFactory.decodeByteArray(img1, 0, img1.length);
					pic.setImageBitmap(b1);
			
					
					//pic.setImageBitmap(bm)
				}
				
				
			} while(c.moveToNext());
		
			
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		openDB();
		display();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}

}
