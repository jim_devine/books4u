package com.example.books4u;

import java.io.IOException;
import java.util.Collections;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class Sell_Main extends Activity {

	Button home, logout, scan, isbn, manual, back;
	DBAdapter myDb;
	SharedPreferences sp;
	public static final String filename = "SharedPrefs";
	String scanned;
	private static final String BOOKINFO = "<div class=\"bookinfo\">";
	private static final String THUMBURL = "<div class=\"thumbnail\">";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sell_main);
		
		openDB();
		
		 sp = getSharedPreferences(filename, 0);
		
		 home = (Button)findViewById(R.id.home_button);
		 logout = (Button)findViewById(R.id.logout);
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
		
		 scan = (Button) findViewById(R.id.button1);
		 isbn = (Button) findViewById(R.id.button2);
		 manual = (Button) findViewById(R.id.button3);
		 back = (Button) findViewById(R.id.button4);
		
		scan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				callBarcodeScanner();
				
					
			}
		});
		
		isbn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					popUp(); //gets isbn...fill fields from there
			}
		});
		
		manual.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.SELL_1");
					startActivity(intent);
			}
		});
		
		
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					finish();
			}
		});
	}

	//@Override
/*public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sell__main, menu);
		return true;
	}*/
	
	public void popUp(){
		final AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("ISBN: ");
		
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Enter", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
			final String value = input.getText().toString().trim();
			
			Toast.makeText(Sell_Main.this, "Searching for Textbook...", Toast.LENGTH_SHORT).show();
			queryISBNSearch(value);
			
			
			
			
			
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
	}
	
	  private void callBarcodeScanner() {
	        IntentIntegrator integrator = new IntentIntegrator(Sell_Main.this);
	        integrator.addExtra("PROMPT_MESSAGE", "Scan the ISBN barcode, or press Back button to stop scanning");
	        integrator.addExtra("RESULT_DISPLAY_DURATION_MS", "1L");
	        integrator.initiateScan(Collections.singleton("EAN_13"));
	    }
	  @Override
	    protected void onActivityResult(int requestCode, int resultCode,
	                                    Intent intent) {
	        super.onActivityResult(requestCode, resultCode, intent);

	        openDB();
	        
	        IntentResult scanResult = IntentIntegrator.parseActivityResult(
	                requestCode, resultCode, intent);
	        if (scanResult != null) {
	            // Scan next barcode if the user does not cancel the previous
	            // scanning action
	            if (scanResult.getContents() != null) {
	            	
	               // scannedIsbns.add(scanResult.getContents());
	                
	                scanned = scanResult.getContents();
	                
	               queryISBNSearch(scanned);
	               
	               
	                  System.out.println(scanned);
	                
	                Cursor c = myDb.getAllBooks();
	                
	                if(c.moveToFirst()){
	                	do{
	                		if(c.getString(1).equals(scanned)){
	                			 Editor editor = sp.edit();
	                			 editor.putString("isbn", scanned);
	                			 editor.putString("author", c.getString(3));
	                			 editor.putString("title", c.getString(2));
	                			 editor.putString("edition", c.getString(4));
	                			 editor.putString("price", "10.0");
	                			 editor.commit();
	                			
	                		}
	                		
	                		
	                	}while(c.moveToNext());
	                }
	                
	        		Intent in = new Intent("com.example.books4u.SELL_2");
	        		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(in);
	                
	            
	            }
	        }
	    }
	
	  private void queryISBNSearch(String query) {
			// Create a new HttpClient and Post Header

		    try {
		    	
		    	HttpClient client = new DefaultHttpClient();
		    	HttpGet request = new HttpGet("http://www.isbnsearch.org/isbn/" + query);
		    	HttpResponse response = client.execute(request);
		        
		    	HttpEntity entity = response.getEntity();
		    	String theResponse = EntityUtils.toString(entity, "UTF-8");
		    	//System.out.println(theResponse);
		    	
		    
		      //  String theResponse = inputStreamToString(response.getEntity().getContent());
		        
		    	
		    	
		    	//String theResponse = response.getEntity().getContent().toString().trim();
		        //Log.v("BookBar", theResponse);
		    	//String theResponse = EntityUtils.toString(theResponse.getEntity());
		        //System.out.println(theResponse);
		        if(theResponse.contains(BOOKINFO)){
		        	System.out.println("in here");
		        	int startThumb = theResponse.indexOf(THUMBURL);
		        	int endThumb = theResponse.indexOf("</div>", startThumb);
		        	
		        	String thumbarea = theResponse.substring(startThumb, endThumb);
		        	
		        	String[] thumbInfo = thumbarea.split("\" alt");
		        	String thumbUrl = thumbInfo[0];
		        	
		        	thumbUrl = thumbUrl.replace("<img src=\"", "");
		        	thumbUrl = thumbUrl.replace("<div class=\"thumbnail\">", "");
		        	
		        	//Log.v("BookBar", "thumbnail at " + thumbUrl);
		        	
		        	
       			// editor.commit();
		        	
		        	int startIndex = theResponse.indexOf(BOOKINFO) + BOOKINFO.length();
		        	int endIndex = theResponse.indexOf("</div>", startIndex);
		        	
		        	String bookinfo = theResponse.substring(startIndex, endIndex);
		        	
		        	String[] titleAndRest = bookinfo.split("</h2>");
		        	
		        	String title = titleAndRest[0];
		        	title = title.replace("<h2>", "");
		        	//Log.v("BookBar", "Title = " + title);
		        	Editor editor = sp.edit();
       			 editor.putString("isbn", scanned);
       			 
       			 editor.putString("title", title );
       			 editor.putString("price", "10.0");
		        	System.out.println(title);
		        	String rest = titleAndRest[1];
		        	
		        	String[] splitFields = rest.split("</p>");
		        	
		        	for(int i = 0; i < splitFields.length; i++){
		        		if(splitFields[i].contains("</strong> ")){
		        					        			
		        			String[] splitInfo = splitFields[i].split("</strong> ");
		        			String fieldTitle = splitInfo[0];
		        			fieldTitle = fieldTitle.replace("<p><strong>", "");
		        			String fieldInfo = splitInfo[1];
		        			//System.out.println(fieldTitle + " " + fieldInfo);
		        			if(fieldTitle.contains("Author")){		        				     				
		        					System.out.println(fieldInfo);
		        					editor.putString("author", fieldInfo);
		        				
		        				}
		        			else if (fieldTitle.contains("Edition")){
		        				System.out.println(fieldInfo);
		        				 editor.putString("edition", fieldInfo);
		        			}
		        				
		        			
		        			/*if(fieldTitle.contains("Title")){
		        				Log.v("BookBar", fieldTitle + " is " + fieldInfo);
		        			}*/
		        				
		        			
		        		}
		        	}
		        	
		        	editor.commit();
		        	
		    Intent in = new Intent("com.example.books4u.SELL_2");
    		in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(in);
		        	
		        }
		        
		        else {
		        	Toast.makeText(Sell_Main.this, "Book not found!", Toast.LENGTH_SHORT).show();
		        	//Log.v("BookBar", "Book not found!");
		        }
		        
		    } catch (ClientProtocolException e) {
		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    }
		    
		    
		    //System.exit(0);
		}
	  
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	

}
