package com.example.books4u;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Splash extends Activity {
TextView text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		Thread timer = new Thread(){
			
			public void run(){
				try{
					sleep(4000);
				}catch (InterruptedException e){
					e.printStackTrace();
					
				}finally{
					
					Intent openStartingPoint = new Intent("com.example.books4u.LOGIN");
					startActivity(openStartingPoint);
					
				}
				
			}			
			
		};
		timer.start();		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	
	
	
	

}
