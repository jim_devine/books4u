package com.example.books4u;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


//Buttons 75px by 75px
public class Home extends Activity {

Button buyText, sellText, myList, myProf, mail, logout, home;	
TextView welcome;
public static final String filename = "SharedPrefs";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		SharedPreferences sp = getSharedPreferences(filename, 0);
		String userName = sp.getString("name", "Does not exist");
		
		
		
		welcome = (TextView)findViewById(R.id.textView1);
		
		welcome.setText("Welcome, " + userName);
		
		buyText = (Button)findViewById(R.id.buyTextbooks);
		sellText = (Button)findViewById(R.id.sellTextbooks);
		home = (Button)findViewById(R.id.home_button);
		myList = (Button)findViewById(R.id.my_listings);
		myProf = (Button)findViewById(R.id.my_profile);
		mail = (Button)findViewById(R.id.mail);
		logout = (Button)findViewById(R.id.logout);
		
		buyText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.SEARCH_2");
				startActivity(openStartingPoint);
								
			}
		});
		
		myProf.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.PROFILE");
				startActivity(openStartingPoint);
								
			}
		});
		
		myList.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.MYLISTINGS");
				startActivity(openStartingPoint);
								
			}
		});
		
		mail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.MYMESSAGES");
				startActivity(openStartingPoint);
								
			}
		});
		
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
		
		sellText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.SELL_MAIN");
				startActivity(openStartingPoint);
			}
		});
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
	}	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
