package com.example.books4u;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Search_2 extends Activity{
	DBAdapter myDb;
	private AlertDialog.Builder builder;
	AlertDialog signal;
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	LinearLayout ll;
	TextView searchText, noResults;
	Button search, back, home, logout;
	ScrollView results;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_2);
		builder = new AlertDialog.Builder(this);
		 signal = builder.create();
		
		 
		 
		 
		 openDB();
		 sp = getSharedPreferences(filename, 0);
		 
		ll = (LinearLayout)findViewById(R.id.search_2_linearlayout);
		
		search = (Button)findViewById(R.id.edit);
		home = (Button)findViewById(R.id.home_button);
		logout = (Button)findViewById(R.id.logout);
		searchText = (EditText)findViewById(R.id.search_text);
		results = (ScrollView)findViewById(R.id.scrollView1);
		//back = (Button)findViewById(R.id.search_2_back);
		
		search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*if(searchText.getText().toString().matches("")){
					signal.setMessage("Please enter a search item!");
					signal.show();
					signal.setCanceledOnTouchOutside(true);
				}*/
				String search = searchText.getText().toString();
				String max = "";
				String min = "";
				
				if(search.matches("")){
					signal.setMessage("Please enter a search item!");
					signal.show();
					signal.setCanceledOnTouchOutside(true);
					
				}
				else{
				
					Editor editor = sp.edit();
					editor.putString("search", search);
					editor.putString("minPrice", min);
					editor.putString("maxPrice", max);
					editor.commit();
					
					
					//Just had to clear the linear layout first and then bring getResults() into this onClickListener
					ll.removeAllViewsInLayout();  
					getResults(); 
				
						
				}
				
			}
		});
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
		
		/*
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		*/
		
			
	}
	
	
	
	@SuppressLint("DefaultLocale")
	public void getResults(){
		
		int results= 0;
		
		Cursor c = myDb.getAllRows(2);
		
		if (c.moveToFirst()) {
			do {
					final Button b = new Button(this);
					
					String searched = sp.getString("search", "Couldn't find");
					String min = sp.getString("minPrice", "Not there");
					String max = sp.getString("maxPrice", "Not there");
					searched = searched.toLowerCase();
					
					
					if((c.getString(1).toLowerCase().contains(searched)) || (c.getString(2).toLowerCase().contains(searched))
					|| (c.getString(3).toLowerCase().contains(searched)) || (c.getString(4).toLowerCase().contains(searched))
					|| (c.getString(5).toLowerCase().contains(searched)))					 
					{
						results++;
							
						b.setText("Seller: " + c.getString(1) + ", Title \"" + c.getString(2) + "\", Author: " + c.getString(3) + ", Price: $" + c.getDouble(7) 
								+ ", Class: " + c.getString(5) + ", ISBN: " + c.getString(4));
					
						System.out.println("Course " + c.getString(5));
						
						ll.addView(b);
					
						final String id = c.getString(0);
					
						//final String message = myDb.getEverything(id);
						//final String message = "";
						
						b.setOnClickListener(new View.OnClickListener() {
						
							@Override
							public void onClick(View v) {
							// TODO Auto-generated method stub							
							
								/*signal.setMessage(message);
								signal.show();
								signal.setCanceledOnTouchOutside(true);*/
								
								long rowId = Long.parseLong(id);
								Cursor cur = myDb.getRow(rowId, 2);
								
								final String seller = cur.getString(1);
								
								String message = "Seller: " +cur.getString(1);
								message += "\nTitle: " +cur.getString(2);
								message += "\nAuthor: " +cur.getString(3);
								message += "\nISBN: " + cur.getString(4);
								message += "\nClass: " + cur.getString(5);
								message += "\nPrice: $" + cur.getDouble(7);
								message += "\n\n" + cur.getString(6);
								builder.setMessage(message);
								builder.setNegativeButton("Message Seller", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										
										Editor editor = sp.edit();
										editor.putString("message_receiver", seller);
										editor.commit();
										
										Intent intent = new Intent("com.example.books4u.COMPOSE");
										startActivity(intent);
										
										
									}
								});
								builder.setTitle("Book Info");
								builder.setPositiveButton("OK",null);
								builder.show(); 
							
							}
						});
												
						
					
						}
									
					
				//if(c.getString(COL_EMAIL).equals(email) && c.getString(COL_PASSWORD).equals(password)){
				//	System.out.println(email);
			//	}				
				
			} while(c.moveToNext());
		}
			
		
		//check if there are no results
		if(results ==0){
			noResults = new TextView(this); 
			noResults.setText("No Results Found");
			ll.addView(noResults);
			
		}
		
		c.close();	
		
		
		
		
		
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}

}
