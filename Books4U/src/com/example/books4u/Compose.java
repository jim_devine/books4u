package com.example.books4u;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Compose extends Activity {

	EditText et_receiver, et_message;
	Button send, back;
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	DBAdapter myDb;
	private AlertDialog.Builder builder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.compose);
		sp = getSharedPreferences(filename, 0);
		builder = new AlertDialog.Builder(this);
		
		back = (Button)findViewById(R.id.compse_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		send = (Button)findViewById(R.id.send);
		et_receiver = (EditText)findViewById(R.id.the_receiver);
		et_receiver.setText(sp.getString("message_receiver", null));
		
		et_message = (EditText)findViewById(R.id.the_message);	
		
		openDB();
			
		send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String sender = sp.getString("name", null);
				String message = et_message.getText().toString().trim();
				String receiver = et_receiver.getText().toString().trim();
				
				if(message.equals("")){
					
					builder.setMessage("Cannot send empty message!");
					builder.setPositiveButton("Ok", null);
					builder.show();
				}
				else if(receiver.equals("")){
					builder.setMessage("Must specify message receiver!");
					builder.setPositiveButton("Ok", null);
					builder.show();
					
				}
				else{
					
					Cursor c = myDb.getAllRows(1);
					boolean valid = false;
					
					if (c.moveToFirst()) {
						do {
							
							if(c.getString(1).equals(receiver)){
								valid = true;
								break;
							}
							
							
						} while(c.moveToNext());
					}
					
					if(valid){
					
						myDb.insertRowMessages(sender, receiver, message, "0", "0");
						
					builder.setMessage("Message Sent!");
					builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							finish();
						}
					});
					
					builder.show();
					}
					
					else{
						
						builder.setMessage("Message receiver does not exist!");
						builder.setPositiveButton("Ok", null);
						builder.show();
						
					}
					
					
					
					
				}
				
				
				
				
			}
		});
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	
	

}
