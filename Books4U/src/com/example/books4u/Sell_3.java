package com.example.books4u;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Sell_3 extends Activity{
String other = "";
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	Editor editor;
	
	Button next, back;
	EditText department, description;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sell_3);
		sp = getSharedPreferences(filename, 0);
		 editor = sp.edit();
		other = sp.getString("other", "Doesn't Exist");
		
		
		department = (EditText)findViewById(R.id.get_department);
		description = (EditText)findViewById(R.id.get_descrption);
		next = (Button)findViewById(R.id.sell_3_next);
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(!department.getText().toString().equals(""))
					other += "Department: " + department.getText().toString() + "\n";
				
				if(!description.getText().toString().equals(""))
					other += "Description: " + description.getText().toString() + "\n";
				
				editor.remove("other");
				System.out.println("other " + other);
				editor.putString("other", other);
				editor.commit();
				
				Intent openStartingPoint = new Intent("com.example.books4u.SELL_4");
				startActivity(openStartingPoint);
				
			}
		});
		
		back = (Button)findViewById(R.id.sell_3_back);
		back.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
			}
		});
	}
	

}
