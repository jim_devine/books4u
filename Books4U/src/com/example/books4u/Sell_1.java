package com.example.books4u;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Sell_1 extends Activity  {
Button next, back, home, logout;
public static final String filename = "SharedPrefs";
SharedPreferences sp;
EditText title, author, isbn, price;
private AlertDialog.Builder builder;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sell_1);
		builder = new AlertDialog.Builder(this);
		
		sp = getSharedPreferences(filename, 0);
		
		title = (EditText)findViewById(R.id.get_title);
		author = (EditText)findViewById(R.id.get_author);
		isbn = (EditText)findViewById(R.id.get_isbn);
		price = (EditText)findViewById(R.id.get_price);
		
		home = (Button)findViewById(R.id.home_button);
		logout = (Button)findViewById(R.id.logout);
		back = (Button)findViewById(R.id.sell_1_back);
		back.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
			}
		});
		
		next = (Button)findViewById(R.id.sell_1_next);
		
next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(title.getText().toString().equals("") || author.getText().toString().equals("") ||
					isbn.getText().toString().equals("") || price.getText().toString().equals("")){
					
					builder.setMessage("All fields are required to proceed!");
					builder.setTitle("Need more info");
					builder.setPositiveButton("OK",null);
					builder.show();
					
					
				}
				else{
					
				Editor editor = sp.edit();
				editor.putString("title", title.getText().toString());
				editor.putString("author", author.getText().toString());
				editor.putString("isbn", isbn.getText().toString());
				editor.putString("price", price.getText().toString());
				editor.commit();
				
				
				Intent openStartingPoint = new Intent("com.example.books4u.SELL_2");
				startActivity(openStartingPoint);
				}
				
				
				
			}
		});
		
		
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
		
	}

	

}
