package com.example.books4u;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends Activity {

Button login, newUser, forgot;
EditText eMail, pWord;
String paused = "";
DBAdapter myDb;
private AlertDialog.Builder builder;
AlertDialog signal;
public static final String filename = "SharedPrefs";
SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.login);
		
		
		 sp = getSharedPreferences(filename, 0);
		
				
		
		builder = new AlertDialog.Builder(this);
		//builder.setPositiveButton("OK",null);
		 signal = builder.create();
		forgot = (Button)findViewById(R.id.forgot_password);
		login = (Button)findViewById(R.id.login);
		newUser = (Button)findViewById(R.id.new_user);
		eMail = (EditText)findViewById(R.id.enter_email);
		pWord = (EditText)findViewById(R.id.enter_password);
		openDB();
		
		
		
		forgot.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
								
				Intent openStartingPoint = new Intent("com.example.books4u.FORGOTPASSWORD");
				startActivity(openStartingPoint);
				
					
				
			}
		});
		
		newUser.setOnClickListener(new View.OnClickListener() {
		
		
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("com.example.books4u.NEWUSER");
				startActivity(openStartingPoint);
			}
		});
		
		login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try{
		
					if(myDb.containsEmail(eMail.getText().toString(), pWord.getText().toString())){
						System.out.println("It in there");
						
						Editor editor = sp.edit();
						editor.putString("name", myDb.getName(eMail.getText().toString()));
						editor.commit();
						
						
						Intent openStartingPoint = new Intent("com.example.books4u.HOME");
						startActivity(openStartingPoint);
			
					}
					else{
						
						
						builder.setTitle("Invalid Credentials");
						builder.setMessage("Please check email and password");
						builder.setPositiveButton("OK",null);
						builder.show(); 
						
					}
						
					} catch(Exception e){
						System.out.println(e.toString());
					}finally{
						System.out.println("It worked!");
					}
				
				
				
			}
		});
	}
	
	@Override
	protected void onSaveInstanceState (Bundle outState) {
	    super.onSaveInstanceState(outState);
	    //String result = "hello";
		//outState.putString("result", result );
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		//String result = savedInstanceState.getString("result");
		//et.setText("helloworld");
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	
	
	

}
