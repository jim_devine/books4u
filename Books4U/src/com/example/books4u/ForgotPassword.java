package com.example.books4u;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ForgotPassword extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password);
		
		Button send = (Button) findViewById(R.id.send);
		Button back = (Button) findViewById(R.id.back);
		final EditText email = (EditText) findViewById(R.id.enter_email);
		TextView result = (TextView) findViewById(R.id.result);
		
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					finish();
			}
		});
		
		send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					String value = email.getText().toString();
					//get password from db and send email
					Toast.makeText(ForgotPassword.this, "Confirmation Email Sent!", Toast.LENGTH_SHORT).show();
			}
		});
		
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forgot_password, menu);
		return true;
	}*/

}
