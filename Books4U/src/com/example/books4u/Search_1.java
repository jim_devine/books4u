package com.example.books4u;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Search_1 extends Activity {
Button back, search, home, logout;
private AlertDialog.Builder builder;
AlertDialog signal;
EditText searchText, minPrice, maxPrice;
Button myList;
public static final String filename = "SharedPrefs";
SharedPreferences sp;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_1);
		
		 sp = getSharedPreferences(filename, 0);
		
		back = (Button)findViewById(R.id.back);
		home = (Button)findViewById(R.id.home_button);
		logout = (Button)findViewById(R.id.logout);
		searchText = (EditText)findViewById(R.id.search_text);
		minPrice = (EditText)findViewById(R.id.min_price);
		maxPrice = (EditText)findViewById(R.id.max_price);
		
		search = (Button)findViewById(R.id.search);
		builder = new AlertDialog.Builder(this);
		 signal = builder.create();
		
		back.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				//finishActivity(0);				
			}
		});
		
		search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*if(searchText.getText().toString().matches("")){
					signal.setMessage("Please enter a search item!");
					signal.show();
					signal.setCanceledOnTouchOutside(true);
				}*/
				String search = searchText.getText().toString();
				String max = maxPrice.getText().toString();
				String min = minPrice.getText().toString();
				
				if(search.matches("")){
					signal.setMessage("Please enter a search item!");
					signal.show();
					signal.setCanceledOnTouchOutside(true);
					
				}
				else{
				
					Editor editor = sp.edit();
					editor.putString("search", search);
					editor.putString("minPrice", min);
					editor.putString("maxPrice", max);
					editor.commit();
					
					Intent intent = new Intent("com.example.books4u.SEARCH_2");
					startActivity(intent);
						
				}
				
			}
		});
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
	
	}


}
