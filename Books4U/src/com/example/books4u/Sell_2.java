package com.example.books4u;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Sell_2 extends Activity {
Button next, back, home, logout;
public static final String filename = "SharedPrefs";
SharedPreferences sp;
EditText edition, course, department;
RadioGroup condition;
String other = "Other Information: \n";

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sell_2);
		
		
		sp = getSharedPreferences(filename, 0);
		
		edition = (EditText)findViewById(R.id.get_edition);
		edition.setText(sp.getString("edition", ""));
		condition = (RadioGroup)findViewById(R.id.get_condition);
		course = (EditText)findViewById(R.id.get_class);
		department = (EditText)findViewById(R.id.get_department);
		
		home = (Button)findViewById(R.id.home_button);
		logout = (Button)findViewById(R.id.logout);
		back = (Button)findViewById(R.id.sell_2_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		next = (Button)findViewById(R.id.sell_2_next);
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Editor editor = sp.edit();
				
				if(!edition.getText().toString().equals(""))
					other += "Edition: " + edition.getText().toString() + "\n";
				
				int selected = condition.getCheckedRadioButtonId();
				RadioButton b = (RadioButton) findViewById(selected);
				other += "Condition: " + b.getText().toString() + "\n";
				
				if(!course.getText().toString().equals(""))
					editor.putString("course", course.getText().toString());
				
				if(!department.getText().toString().equals(""))
					other += "Department: " + department.getText().toString() + "\n";
				
				System.out.println("other " + other);
			
				editor.putString("other", other);
				editor.commit();
				
				
				Intent openStartingPoint = new Intent("com.example.books4u.SELL_4");
				startActivity(openStartingPoint);
				
			}
		});
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		other = "Other Information: \n";
		
	}
	
	

}
