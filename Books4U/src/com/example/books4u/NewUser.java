package com.example.books4u;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewUser extends Activity {
Button back, submit;
private AlertDialog.Builder builder;
AlertDialog signal;
EditText etName, etEmail, etPassword, etConfirm;
DBAdapter myDb;
public static final String filename = "SharedPrefs";
SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_user);
		
		 sp = getSharedPreferences(filename, 0);

		
		
		openDB();
		
		builder = new AlertDialog.Builder(this);
		signal = builder.create();
		
		submit = (Button)findViewById(R.id.newuserSubmit);
		back = (Button)findViewById(R.id.newuserBack);
		etName = (EditText)findViewById(R.id.get_username);
		etEmail = (EditText)findViewById(R.id.get_email);
		etPassword = (EditText)findViewById(R.id.get_password);
		etConfirm = (EditText)findViewById(R.id.confirm_password);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				
			}
		});
		
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*signal.setMessage("Confirmation Email Sent!");
				signal.show();
				signal.setCanceledOnTouchOutside(true);
				Toast.makeText(NewUser.this, "Confirmation Email Sent", Toast.LENGTH_LONG).show();*/
				
				
				if(etName.getText().toString().equals("") || etEmail.getText().toString().equals("")
						|| etPassword.getText().toString().equals("") || etConfirm.getText().toString().equals("")){
					
					builder.setMessage("All fields are required!");
					builder.setTitle("Error");
					builder.setPositiveButton("OK",null);
					builder.show(); 
					
				}
				
				else if(!etEmail.getText().toString().contains("@pitt.edu")){
					builder.setMessage("Must use a Pitt email!");
					builder.setTitle("Error");
					builder.setPositiveButton("OK",null);
					builder.show();  
					
					
				}
				
				else if(!etConfirm.getText().toString().equals(etPassword.getText().toString())){
					
					
					builder.setMessage("Passwords do not match!");
					builder.setTitle("Error");
					builder.setPositiveButton("OK",null);
					builder.show(); 
					
				}
										
				else{
					
					
					
					
					Bitmap b=BitmapFactory.decodeResource(getResources(), R.drawable.profile_icon);
			        ByteArrayOutputStream bos=new ByteArrayOutputStream();
			        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
			        byte[] img=bos.toByteArray();   
					
					myDb.insertRowUsers(etName.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString(), " ", img);
					
					if(myDb.containsEmail(etEmail.getText().toString(), etPassword.getText().toString())){
										
					
					/*Intent email = new Intent(android.content.Intent.ACTION_SEND);
					email.putExtra(android.content.Intent.EXTRA_EMAIL, etEmail.getText().toString());
					email.setType("plain/text");
					email.putExtra(android.content.Intent.EXTRA_SUBJECT, "Books4U Confirmation");
					email.putExtra(android.content.Intent.EXTRA_TEXT, "Welcome to Books4U");
					startActivity(email);*/
					
					Editor editor = sp.edit();
					editor.putString("name", myDb.getName(etEmail.getText().toString()));
					editor.commit();
					
					
						Intent openStartingPoint = new Intent("com.example.books4u.REGISTERSUCCESS");
						startActivity(openStartingPoint);
					}
					else{
						signal.setMessage("Didn't work");
						signal.show();
						signal.setCanceledOnTouchOutside(true);
					}
					
				}
				
							
			} 
		}); 
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		closeDB();
		finish();
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		openDB();
	}

	

}
