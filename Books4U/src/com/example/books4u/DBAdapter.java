package com.example.books4u;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {

	private static final String TAG = "DBAdapter";
	public static final String KEY_ROWID = "_id";
	public static final int COL_ROWID = 0;

	public static final String KEY_NAME = "name";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_PASSWORD = "password";
	
	public static final int COL_NAME = 1;
	public static final int COL_EMAIL= 2;
	public static final int COL_PASSWORD = 3;

	
	public static final String[] ALL_KEYS = new String[] {KEY_ROWID, KEY_NAME, KEY_EMAIL, KEY_PASSWORD};
	
	public static final String DATABASE_NAME = "Books4uDb";
	public static final String DATABASE_TABLE = "userTable";
	
	public static final int DATABASE_VERSION = 2;	
	
	private static final String DATABASE_CREATE_SQL = 
			"create table " + DATABASE_TABLE 
			+ " (" + KEY_ROWID + " integer primary key autoincrement, "
			
			+ KEY_NAME + " text not null, "
			+ KEY_EMAIL + " string not null, "
			+ KEY_PASSWORD + " string not null"
			
			+ ");";
	
	public static final String DATABASE_TABLE_BOOKS = "booksTable";
	
	public static final String KEY_ROWID_BOOKS = "_id";
	public static final int COL_ROWID_BOOKS = 0;
	
	public static final String KEY_SELLER = "seller";
	public static final String KEY_TITLE = "title";
	public static final String KEY_AUTHOR = "author";
	public static final String KEY_ISBN = "isbn";
	public static final String KEY_CLASS = "course";
	public static final String KEY_OTHER = "other";
	public static final String KEY_PRICE = "price";
	
	public static final int COL_SELLER = 1;
	public static final int COL_TITLE = 2;
	public static final int COL_AUTHOR= 3;
	public static final int COL_ISBN = 4;
	public static final int COL_CLASS = 5;
	public static final int COL_OTHER = 6;
	public static final int COL_PRICE = 7;
	
	
	public static final String[] ALL_KEYS_BOOKS = new String[] {KEY_ROWID_BOOKS, KEY_SELLER, KEY_TITLE, KEY_AUTHOR, KEY_ISBN, KEY_CLASS, KEY_OTHER, KEY_PRICE};
	
	private static final String DATABASE_CREATE_SQL_BOOKS = 
			"create table " + DATABASE_TABLE_BOOKS 
			+ " (" + KEY_ROWID_BOOKS + " integer primary key autoincrement, "
			
			
			+ KEY_SELLER + " text not null, "
			+ KEY_TITLE + " string not null, "
			+ KEY_AUTHOR + " string not null, "
			+ KEY_ISBN + " string not null, "
			+ KEY_CLASS + " string, "
			+ KEY_OTHER + " string not null, "
			+ KEY_PRICE + " real not null" + ");";
	
	
	
	// Context of application who uses us.
	private final Context context;
	
	private DatabaseHelper myDBHelper;
	private SQLiteDatabase db;
	
	public DBAdapter(Context ctx) {
		this.context = ctx;
		myDBHelper = new DatabaseHelper(context);
	}

	public DBAdapter open() {
		db = myDBHelper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		myDBHelper.close();
		
	}
	
	public long insertRowUsers(String name, String email, String password, String description, byte[] img){
		
		ContentValues initialValues = new ContentValues();
		
		initialValues.put("name", name);
		initialValues.put("email", email);
		initialValues.put("password", password);
		initialValues.put("description", description);
		initialValues.put("image", img);

		return db.insert("usersTable", null, initialValues);
		
	}
	
	public long insertRow(String name, String email, String password) {
	
		ContentValues initialValues = new ContentValues();
		
		initialValues.put(KEY_NAME, name);
		initialValues.put(KEY_EMAIL, email);
		initialValues.put(KEY_PASSWORD, password);

		return db.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long insertRowBooks(String seller, String title, String author, String isbn, String course, String other, double price) {
		
		ContentValues initialValues = new ContentValues();
		
		initialValues.put(KEY_SELLER, seller);
		
		initialValues.put(KEY_TITLE, title);
		initialValues.put(KEY_AUTHOR, author);
		initialValues.put(KEY_ISBN, isbn);
		initialValues.put(KEY_CLASS, course);
		initialValues.put(KEY_OTHER, other);
		initialValues.put(KEY_PRICE, price);
		
		// Insert it into the database.
		return db.insert(DATABASE_TABLE_BOOKS, null, initialValues);
	}
	
	public long insertRowAllBooks(String isbn, String title, String author, String edition){
		
		ContentValues initialValues = new ContentValues();
		
		initialValues.put("isbn", isbn);		
		initialValues.put("title", title);
		initialValues.put("author", author);
		initialValues.put("edition", edition);
		
		return db.insert("allbooks", null, initialValues);
	}
	
public long insertRowMessages(String sender, String receiver , String message, String num1, String num2) {
		
		ContentValues initialValues = new ContentValues();
		
		initialValues.put("sender", sender);
		initialValues.put("receiver", receiver);
		initialValues.put("message", message);		
		initialValues.put("num1", num1);
		initialValues.put("num2", num2);
		
		// Insert it into the database.
		return db.insert("Messages", null, initialValues);
	}

public boolean containsSender(String sender){
	Cursor c = this.getAllRowsMessages();
	if (c.moveToFirst()) {
		do {
			
			if(c.getString(1).equals(sender)){
				return true;
			}				
			
		} while(c.moveToNext());
	}
			
	c.close();		
	
	
	
	return false;
}

	
	public void insert(ContentValues cv){
		
		//db.execSQL("create table tableimage(image blob);");
		db.insert("tableimage", null, cv);
		
		
	}
	
	
	
	
	
	public boolean deleteRow(long rowId, int table) {
		if(table == 1){
			String where = "_id" + "=" + rowId;
			return db.delete("usersTable", where, null) != 0;
		}
		else if (table == 2){
			String where = KEY_ROWID_BOOKS + "=" + rowId;
			return db.delete(DATABASE_TABLE_BOOKS, where, null) != 0;
		}
		else{
			String where = "m_id" + "=" + rowId;
			return db.delete("Messages", where, null) != 0;
		}
			
	}
	
	public void deleteAll(int table) {
		Cursor c = getAllRows(table);
		
		long rowId;
		
		if(table == 1){
			rowId = c.getColumnIndexOrThrow(KEY_ROWID);
		}
		else
			rowId = c.getColumnIndexOrThrow(KEY_ROWID_BOOKS);	
		
		if (c.moveToFirst()) {
			do {
				deleteRow(c.getLong((int) rowId), table);				
			} while (c.moveToNext());
		}
		c.close();
	}

	public Cursor getAllRows(int table) {
		Cursor c = null;
		if(table == 1){
			String where = null;
			String [] arr = {"_id", "name", "email", "password", "description", "image"};
			 c = 	db.query(true, "usersTable", arr, 
							where, null, null, null, null, null);
			if (c != null) {
				c.moveToFirst();
			}
		}
		else if(table == 2){
			String where = null;
			 c = 	db.query(true, DATABASE_TABLE_BOOKS, ALL_KEYS_BOOKS, 
							where, null, null, null, null, null);
			if (c != null) {
				c.moveToFirst();
			}
		}
			
			return c;
	}
	
	public Cursor getAllBooks(){
		Cursor c = null;
		String [] arr = {"b_id", "isbn", "title", "author", "edition"};
		String where = null;
		 c = 	db.query(true, "allbooks", arr, 
						where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}		
		return c;
	}
	
	public Cursor getAllRowsImages(){
		Cursor c = null;
		String [] arr = {"image"};
		String where = null;
		 c = 	db.query(true, "tableimage", arr, 
						where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}		
		return c;		
	}
	
	public Cursor getAllRowsMessages(){
		
		Cursor c = null;
		String [] arr = {"m_id", "sender", "receiver", "message", "num1", "num2"};
		String where = null;
		 c = 	db.query(true, "Messages", arr, 
						where, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}		
		return c;
		
	}

	public Cursor getRow(long rowId, int table) {
		Cursor c = null;
		String [] arr = {"_id", "name", "email", "password", "description", "image"};
		if(table == 1){
			String where = "_id" + "=" + rowId;
			c = 	db.query(true, "usersTable", arr, 
						where, null, null, null, null, null);
		 if (c != null) {
			c.moveToFirst();
		 	}
		}
		else{
			String where = KEY_ROWID_BOOKS + "=" + rowId;
			c = 	db.query(true, DATABASE_TABLE_BOOKS, ALL_KEYS_BOOKS, 
						where, null, null, null, null, null);
		 if (c != null) {
			c.moveToFirst();
		 	}
		}
		
		
		return c;
	}
	
	public boolean updateRow(long rowId, String name, String email, String password, String description, byte[] img) {
		String where = "_id" + "=" + rowId;

		ContentValues newValues = new ContentValues();
		newValues.put("name", name);
		newValues.put("email", email);
		newValues.put("password", password);
		newValues.put("description", description);
		newValues.put("image", img);
				
		return db.update("usersTable", newValues, where, null) != 0;
	}
	
	public boolean updateRowBooks(long rowId, String seller, String title, String author, String isbn, String course, String other, double price) {
		String where = KEY_ROWID_BOOKS + "=" + rowId;

		ContentValues newValues = new ContentValues();
		newValues.put(KEY_SELLER, seller);
		newValues.put(KEY_TITLE, title);
		newValues.put(KEY_AUTHOR, author);
		newValues.put(KEY_ISBN, isbn);
		newValues.put(KEY_CLASS, course);
		newValues.put(KEY_OTHER, other);
		newValues.put(KEY_PRICE, price);
				
		return db.update(DATABASE_TABLE_BOOKS, newValues, where, null) != 0;
	}
	
	public boolean updateRowMessages(long rowId, String sender, String receiver, String message, String num1, String num2){
		String where = "m_id = " + rowId;
		ContentValues newValues = new ContentValues();
		newValues.put("sender", sender);
		newValues.put("receiver", receiver);
		newValues.put("message", message);
		newValues.put("num1", num1);
		newValues.put("num2", num2);
		
		return db.update("Messages", newValues, where, null) != 0;
	}
	
	public boolean containsEmail(String email, String password){		
		Cursor c = this.getAllRows(1);
		if (c.moveToFirst()) {
			do {
				
				if(c.getString(COL_EMAIL).equals(email) && c.getString(COL_PASSWORD).equals(password)){
					return true;
				}				
				
			} while(c.moveToNext());
		}
				
		c.close();		
		return false;		
		
	}
	
	public String getName(String email){		
		Cursor c = this.getAllRows(1);
		if (c.moveToFirst()) {
			do {
				
				if(c.getString(COL_EMAIL).equals(email)){
					
					return c.getString(COL_NAME);
				}
					
				
			} while(c.moveToNext());
		}
				
		c.close();		
		return null;		
		
	}
	
	public boolean containsBook(String title){
		Cursor c = this.getAllRows(2);
		if (c.moveToFirst()) {
			do {
				
				if(c.getString(COL_TITLE).equals(title)){
					return true;
				}				
				
			} while(c.moveToNext());
		}
				
		c.close();		
		
		
		
		return false;
	}
	
	public String getEverything(String id){
		Cursor c = this.getAllRows(2);
		if (c.moveToFirst()) {
			do {
				
				if(c.getString(0).equals(id)){
					int i  = 1;
					String everything = " ";
					for(i = 1; i < 7; i++){
						
						everything += c.getString(i) + "\n";
					}
					
					return everything;
					
				}				
				
			} while(c.moveToNext());
		}
				
		c.close();
		
		return null;
	}
	
	public void deleteDatabase(){
		
		context.deleteDatabase(DATABASE_NAME);
		
		
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase _db) {
			//_db.execSQL(DATABASE_CREATE_SQL);
			_db.execSQL("create table usersTable (_id integer primary key autoincrement," +
			" name text not null, email string not null, password string not null," +
					" description string, image blob);");
					
			_db.execSQL("create table allbooks (b_id integer primary key autoincrement, "+
			"isbn string not null, title string not null, author string not null, " +
			"edition string not null);");		
			
			_db.execSQL(DATABASE_CREATE_SQL_BOOKS);
			
			_db.execSQL("create table tableimage(image blob);");
			
			_db.execSQL("create table Messages (m_id integer primary key autoincrement," +
					"  sender string not null, " +
					"receiver string not null, message string not null, num1 string not null, num2 string not null);");
			
		}
		
		

		

		@Override
		public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading application's database from version " + oldVersion
					+ " to " + newVersion + ", which will destroy all old data!");
			
			_db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			_db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_BOOKS);
			_db.execSQL("DROP TABLE IF EXISTS tableimage;");
			_db.execSQL("DROP TABLE IF EXISTS Messages;");
			
			onCreate(_db);
		}
	}
}
