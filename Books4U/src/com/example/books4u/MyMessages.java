package com.example.books4u;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class MyMessages extends Activity implements AdapterView.OnItemSelectedListener{

	Spinner spinner;
	LinearLayout ll;
	Button button;
	View view;
	Button compose, back;
	Button ibutton;
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	DBAdapter myDb;
	private AlertDialog.Builder builder, builder2, builder3;
	String username;
	int mail_options;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_messages);
		
		mail_options = 0;
		
		ll = (LinearLayout)findViewById(R.id.my_messages_linearlayout);
		spinner = (Spinner)findViewById(R.id.spinner1);
		
		sp = getSharedPreferences(filename, 0);
		builder = new AlertDialog.Builder(this);
		builder2 = new AlertDialog.Builder(this);
		builder3 = new AlertDialog.Builder(this);
		
		compose = (Button)findViewById(R.id.compose);
		back = (Button)findViewById(R.id.back_button);
		
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		compose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Editor editor = sp.edit();
				editor.putString("message_receiver","");
				editor.commit();
				
				Intent intent = new Intent("com.example.books4u.COMPOSE");
				startActivity(intent);
				
			}
		});
		
		Button home = (Button)findViewById(R.id.home_button);
		Button logout = (Button)findViewById(R.id.logout);
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
		
		ArrayAdapter adapter =ArrayAdapter.createFromResource(this, R.array.choices, R.layout.spinner_layout);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		
		username = sp.getString("name", null);
		
		
		System.out.println("name " + username);
		
		openDB();
		
		getResults();
	}
	
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		TextView myText = (TextView)arg1;
				
		if(myText.getText().toString().equals("Inbox")){
			//ll.removeAllViews();
			//ll.addView(myText);
			mail_options = 0;
			ll.removeAllViews();
			getResults();
			System.out.println("inbox");
		}
		else if(myText.getText().toString().equals("Sent")){
			//ll.removeAllViews();
			//ll.addView(myText);
			mail_options = 1;
			ll.removeAllViews();
			getResults();
			System.out.println("sent");
		}
		else if(myText.getText().toString().equals("Recently Deleted")){
			//ll.removeAllViews();
			//ll.addView(myText);
		
			System.out.println("recently deleted");
		}
	}
	
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void getResults(){
		
		int results= 0;
		
		Cursor c = myDb.getAllRowsMessages();
		
		if (c.moveToFirst()) {
			do {
					final Button b = new Button(this);
				if(mail_options == 0){
					
					if((c.getString(2).equals(username)) && c.getString(4).equals("0"))					 
					{
	
						b.setText("From: " + c.getString(1));
						
						ll.addView(b);
					
						final String row_id = c.getString(0);
						final String message = c.getString(3);
						
						//final String num_1 = c.getString(4);
						//int num1 = Integer.parseInt(num_1);
						
						
						builder.setPositiveButton("Ok", null);
						
						final String receiver = c.getString(2);
						final String sender = c.getString(1);
						final String num1 = c.getString(4);
						final String num2 = c.getString(5);
						
						
						b.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								builder.setMessage(message);
								
								builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								builder3.setMessage("Are you sure you want to delete this message?");
								builder3.setNegativeButton("Cancel", null);
								builder3.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										long rowId = Long.parseLong(row_id);
										//myDb.deleteRow(rowId, 3);
										if(num2.equals("0")){
											myDb.updateRowMessages(rowId, sender, receiver, message, "1", num2);
										}
										else{
											myDb.deleteRow(rowId, 3);
										}
										
										ll.removeAllViews();
										getResults();
									}
								});
								builder3.show();
							}
						});
								
								
								
								
								builder.setNeutralButton("Reply", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										Editor editor = sp.edit();
										//sender because we're responding to the person who "sent" the message
										editor.putString("message_receiver", sender);
										editor.commit();
										
										Intent intent = new Intent("com.example.books4u.COMPOSE");
										startActivity(intent);
									}
								});
								builder.show();
							}
						});
					
						}					
					}
				
				else if (mail_options == 1){
					
					if((c.getString(1).equals(username)) && c.getString(5).equals("0"))					 
					{
	
						b.setText("To: " + c.getString(2));
						
						ll.addView(b);
					
						final String row_id = c.getString(0);
						final String message = c.getString(3);
						
						final String num1 = c.getString(4);
						final String sender = c.getString(1);
						final String receiver = c.getString(2);
						
						
						b.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								builder2.setMessage(message);
								builder2.setPositiveButton("Ok", null);
								builder2.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										builder3.setMessage("Are you sure you want to delete this message?");
										builder3.setNegativeButton("Cancel", null);
										builder3.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												long rowId = Long.parseLong(row_id);
												//myDb.deleteRow(rowId, 3);
												if(num1.equals("0")){
													myDb.updateRowMessages(rowId, sender, receiver, message, num1, "1");
												}
												else{
													myDb.deleteRow(rowId, 3);
												}
												
												ll.removeAllViews();
												getResults();
											}
										});
										builder3.show();
									}
								});
								builder2.show();
							}
						});
					
						}
				
				}
					
									
					
				//if(c.getString(COL_EMAIL).equals(email) && c.getString(COL_PASSWORD).equals(password)){
				//	System.out.println(email);
			//	}				
				
			} while(c.moveToNext());
		}
			
		
		//check if there are no results
		
		c.close();	
		
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	

}