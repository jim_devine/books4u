package com.example.books4u;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Edit_Listing extends Activity {
	DBAdapter myDb;
	Button back, remove, update;
	private AlertDialog.Builder builder;
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	EditText etprice, etcourse, etdepartment, etcondition, etedition;
	String defValue = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_listing);
		
		sp = getSharedPreferences(filename, 0);
		builder = new AlertDialog.Builder(this);
		openDB();
		
		etprice = (EditText)findViewById(R.id.edit_price);
		etcourse = (EditText)findViewById(R.id.edit_class);
		etdepartment = (EditText)findViewById(R.id.edit_dept);
		etcondition = (EditText)findViewById(R.id.edit_condition);
		etedition = (EditText)findViewById(R.id.edit_edition);
		
		etprice.setText(sp.getString("book_price", null));
		etcourse.setText(sp.getString("book_class", null));
		etdepartment.setText(sp.getString("book_dept", null));
		etcondition.setText(sp.getString("book_condition", null));
		etedition.setText(sp.getString("book_edition", null));
		
		back = (Button)findViewById(R.id.edit_listing_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				finish();
			}
		});
		
		update = (Button)findViewById(R.id.edit_update);
		update.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//long rowId = (long)Integer.parseInt(sp.getString("book_id", null));
				String id = sp.getString("book_id", null);
				
				//System.out.println("edit id " + id);
				
				String seller = sp.getString("book_seller", null);
				
			//	System.out.println("seller " + seller);
				
				String title = sp.getString("book_title", null);
				
				//System.out.println("title " + title);
				
				String author = sp.getString("book_author", null);
				
				//System.out.println("author " + author);
				
				String isbn = sp.getString("book_isbn", null);
				
				//System.out.println("isbn " + isbn);
				//String course = sp.getString("book_class", null);
				String course = etcourse.getText().toString().trim();
				double price = Double.parseDouble(etprice.getText().toString().trim());
				
				String other = "Other Information:  \n" + "Edition: " + etedition.getText().toString().trim();
				other += "\nCondition: " + etcondition.getText().toString().trim();
				other += "\nDepartment: " + etdepartment.getText().toString().trim();
				
				
				//myDb.insertRowBooks("James Devine", "data", "Novacky", "134563", "CS 445", " ", 45.5);
				
				
				//System.out.println("rowid " + rowId);
				//myDb.deleteRow(rowId, 2);
				
				//myDb.insertRowBooks(seller, title, author, isbn, course, other, price);
				long rowId = Long.parseLong(id);
				myDb.updateRowBooks(rowId, seller, title, author, isbn, course, other, price);
				
				//System.out.println("lemme upgrade ya");
				
				finish();
				
			}
		});
		
		remove = (Button)findViewById(R.id.remove_listing);
		remove.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				builder.setMessage("Are you sure you want to delete this listsing?");
				builder.setTitle("Delete Listing");
				
				builder.setPositiveButton("Delete",new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						String id = sp.getString("book_id", null);
						long rowId = Long.parseLong(id);
						myDb.deleteRow(rowId, 2);
						
						finish();
					}
				});
				
				builder.setNegativeButton("Cancel",null);
					
					
				builder.show();
				
				
				//finish();
				
			}
		});
		
		Button home = (Button)findViewById(R.id.home_button);
		Button logout = (Button)findViewById(R.id.logout);
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
	}
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	
	

}
