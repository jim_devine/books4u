package com.example.books4u;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class MyListings extends Activity {
	DBAdapter myDb;
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	private AlertDialog.Builder builder, builder2, removeBook;
	Dialog dialog;
	AlertDialog signal;
	LinearLayout ll;
	TextView title;
	Button back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_listings);
		openDB();
		
		back = (Button)findViewById(R.id.my_listings_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
		sp = getSharedPreferences(filename, 0);
		builder = new AlertDialog.Builder(this);
		builder2 = new AlertDialog.Builder(this);
		removeBook = new AlertDialog.Builder(this);
		dialog = new Dialog(this);
		 signal = builder.create();
		 
		ll = (LinearLayout)findViewById(R.id.my_listings_linearlayout);
		title = (TextView)findViewById(R.id.my_listings_title);
		
		String name = sp.getString("name", "Could't find name");
		title.setText(name + "\'s listings");
		
		Button home = (Button)findViewById(R.id.home_button);
		Button logout = (Button)findViewById(R.id.logout);
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
		
		getListings();
		
	}
	
	
	public void getListings(){
		
		 Cursor c = myDb.getAllRows(2);
		
		if (c.moveToFirst()) {
			do {
					final Button b = new Button(this);
					
					String name = sp.getString("name", "Couldn't find");
					
					if(c.getString(1).toLowerCase().equals(name.toLowerCase())){
						
						b.setText("Seller: " + c.getString(1) + ", Title \"" + c.getString(2) + "\", Author: " + c.getString(3) + ", Price: $" + c.getDouble(7) 
								+ ", Class: " + c.getString(5) + ", ISBN: " + c.getString(4));
					
						//System.out.println("Course " + c.getString(5));
						
						final Double price = c.getDouble(7);
						final String other = c.getString(6);
						final String other_array [] = other.split("\n"); 
												
						ll.addView(b);
															
						//this textview is only added to make space between listings
						final TextView emptyspace = new TextView(this);
						ll.addView(emptyspace);
						
						final String id = c.getString(0);
						
						//System.out.println("first id " + id);
						
						final String book_seller = c.getString(1);
						final String book_title = c.getString(2);
						final String book_author = c.getString(3);
						final String book_class = c.getString(5);
						final String book_isbn = c.getString(4);
						
					/*	Editor editor = sp.edit();
						editor.putString("book_id", id);
						editor.putString("book_seller", c.getString(1));
						editor.putString("book_title", c.getString(2));
						editor.putString("book_author", c.getString(3));
						editor.putString("book_class", c.getString(5));
						editor.putString("book_isbn", c.getString(4));						
						editor.commit();	*/					
					
						//final String message = myDb.getEverything(id);
						b.setOnClickListener(new View.OnClickListener() {
						
							@Override
							public void onClick(View v) {
							// TODO Auto-generated method stub	
								
								
								
						//	final String message = myDb.getEverything(id);
								
							String edition = "";
							 String condition = "";
							 String course_number = "";
							 String department = "";
								//System.out.println(other);
							 
								String [] temp = null;
								for(int i = 0; i < other_array.length; i++){
									if(other_array[i].trim().contains("Edition")){
										edition = other_array[i].trim();
										temp = edition.split(":");
										edition = temp[1].trim();
									}
										
									else if(other_array[i].trim().contains("Class Number")){
										course_number = other_array[i].trim();
										temp = course_number.split(":");
										course_number = temp[1].trim();
									}
										
									else if(other_array[i].trim().contains("Condition")){
										condition = other_array[i].trim();
										temp = condition.split(":");
										condition = temp[1].trim();
									}
										
									else if(other_array[i].trim().contains("Department")){
										department = other_array[i].trim();
										temp = department.split(":");
										department = temp[1].trim();
									}
								}
								
								final String pri = price + " ";
								final String edi = edition;
								final String cond = condition;
								final String course_num = course_number;
								final String dept = department;
								
								/*
								Editor editor2 = sp.edit();
								editor2.putString("book_edition", edition);
								editor2.putString("book_price", pri.trim());
								editor2.putString("book_dept", department);
								editor2.putString("book_condition", condition);
								//editor.putString("book_class", course_number);
								
								editor2.commit();
								
								*/
								long rowId = Long.parseLong(id);
								Cursor cur = myDb.getRow(rowId, 2);
								
								String message = "Seller: " +cur.getString(1);
								message += "\nTitle: " +cur.getString(2);
								message += "\nAuthor: " +cur.getString(3);
								message += "\nISBN: " + cur.getString(4);
								message += "\nClass: " + cur.getString(5);
								message += "\nPrice: $" + cur.getDouble(7);
								message += "\n\n" + cur.getString(6);
								
								builder.setMessage(message);
								builder.setTitle("Book Info");
								builder.setPositiveButton("OK",null);
								
																
								builder.setNegativeButton("Edit/Remove", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										System.out.println("id my listing " + id);
										//System.out.println(message);
											
										Editor editor2 = sp.edit();
										editor2.putString("book_edition", edi);
										editor2.putString("book_price", pri.trim());
										editor2.putString("book_dept", dept);
										editor2.putString("book_condition", cond);
										//editor.putString("book_class", course_number);
										
										editor2.commit();
										
										Editor editor = sp.edit();
										editor.putString("book_id", id);
										editor.putString("book_seller", book_seller);
										editor.putString("book_title", book_title);
										editor.putString("book_author", book_author);
										editor.putString("book_class", book_class);
										editor.putString("book_isbn", book_isbn);						
										editor.commit();		
										Intent intent = new Intent("com.example.books4u.EDIT_LISTING");
										startActivity(intent);
									}
								});
								
								builder.show(); 
							}						
							
						});					
						
					}					
					
			} while(c.moveToNext());
		}
				
		c.close();	
		
	}
	
	
	
	
	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//eMail.setText(paused);
		openDB();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//paused = eMail.getText().toString();
		closeDB();
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();	
		closeDB();
	}
	

}