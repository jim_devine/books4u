package com.example.books4u;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Sell_4 extends Activity {
	public static final String filename = "SharedPrefs";
	SharedPreferences sp;
	Button next, home, logout;	
	private AlertDialog.Builder builder;
	AlertDialog signal;
	DBAdapter myDb;
	Button post, back;
	RelativeLayout rl;
	TextView sellerText, titleText, authorText, isbnText, priceText, otherText;
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sell_4);
		builder = new AlertDialog.Builder(this);
		 signal = builder.create();
		 
		 sp = getSharedPreferences(filename, 0);
		 
		 sellerText = (TextView)findViewById(R.id.text_seller);
		 titleText = (TextView)findViewById(R.id.text_title);
		 authorText = (TextView)findViewById(R.id.text_author);
		 isbnText = (TextView)findViewById(R.id.text_isbn);
		 priceText = (TextView)findViewById(R.id.text_price);
		 otherText = (TextView)findViewById(R.id.text_other);
		 
		final String seller = sp.getString("name", "No Name");
		final String title = sp.getString("title", "No Title");
		final String author = sp.getString("author", "No author");
		final String isbn = sp.getString("isbn", "No isbn");
		final String course = sp.getString("course", "No course");
		final String price = sp.getString("price", "10.00");
		final String other = sp.getString("other", "No other");
		 
		 sellerText.setText("Seller: " + seller);
		 titleText.setText("Title: " + title);
		 authorText.setText("Author: " + author);
		 isbnText.setText("ISBN: " + isbn);
		 priceText.setText("Price: $" + price);
		 otherText.setText(other);
		 
		 //System.out.println("Price " + price);
		 
		home = (Button)findViewById(R.id.home_button); 
		logout = (Button)findViewById(R.id.logout);
		back = (Button)findViewById(R.id.sell_4_back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
							
				finish();
				
			}
		});
		 
	
		 openDB();
		post = (Button)findViewById(R.id.post);
		
		rl = (RelativeLayout)findViewById(R.id.rel_layout);
		
		
		
		
		post.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				Editor editor = sp.edit();
   			 editor.putString("edition", "");   			 
   			 editor.commit();
					
							myDb.insertRowBooks(seller, title, author, isbn, course, other, Double.parseDouble(price));
					
														
							
							Intent intent = new Intent("com.example.books4u.HOME");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							
							
						}			 
		
			
		});
		
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent intent = new Intent("com.example.books4u.HOME");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
			}
		});
	
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				//Intent log_out = new Intent("android.intent.action.MAIN");
				Intent log_out = new Intent("android.intent.action.LOGIN");
				log_out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(log_out);
				
				finish();
				
			}
		});	
	}
	
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		closeDB();
		finish();
	}



	private void openDB() {
		myDb = new DBAdapter(this);
		myDb.open();
	}
	
	private void closeDB() {
		myDb.close();
	}

}
